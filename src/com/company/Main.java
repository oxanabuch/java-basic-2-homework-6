package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        // option 1
        String[] fruits1 = new String[10]; // empty array

        fruits1[0] = "peach";
        fruits1[1] = "strawberry";
        fruits1[2] = "blueberry";
        fruits1[3] = "orange";
        fruits1[4] = "mango";
        fruits1[5] = "fig";
        fruits1[6] = "apple";
        fruits1[7] = "plum";
        fruits1[8] = "grape";
        fruits1[9] = "pear";

        // option 2
        String[] fruits2 = {"peach", "strawberry", "blueberry", "orange", "mango", "fig", "apple", "plum", "grape", "pear"};

        String[] food = Arrays.copyOf(fruits1, fruits1.length);
        System.out.println("Food array elements: ");

        for (int i = 0; i < 10; i++) {
            System.out.println(food[i]);
        }

        // option 1
        int[][] score1 = new int[3][4];
        score1[0][0] = 10;
        score1[0][1] = 11;
        score1[0][2] = 12;
        score1[0][3] = 13;

        score1[1][0] = 20;
        score1[1][1] = 21;
        score1[1][2] = 22;
        score1[1][3] = 23;

        score1[2][0] = 30;
        score1[2][1] = 31;
        score1[2][2] = 32;
        score1[2][3] = 33;

        // option 2
        int[][] score2 = {{10, 11, 12, 13}, {20, 21, 22, 23}, {30, 31, 32, 33}};


        int[] result = new int[score1.length];
        int total;
        System.out.println();
        System.out.println("Array result (new array): ");

        for (int i = 0; i < score1.length; i++) {
            total = 0;
            for (int j = 0; j < score1[i].length; j++) {
                total += score1[i][j];
            }
            result[i] = total;
            System.out.println(total);
        }
    }
}
